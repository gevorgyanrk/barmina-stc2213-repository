package Homework8;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
//Создаем массив worker, заполняем его профессиями
        Worker[] worker = new Worker[4];
        worker[0] = new Tutor("Татьяна ", "Чернова ", "Тьютор");
        worker[1] = new Educator("Владимир ", "Бобров ", "Воспитатель");
        worker[2] = new Psychologist("Екатерина ", "Зайцева ", "Психолог");
        worker[3] = new Teacher("Светлана ", "Зверева ", "Учитель");

        // Выводим каждый элемент массива на печать и выводим в консоль наши методы. Дни отпуска заполняем рандомно
        System.out.println(worker[0]);
        worker[0].goToWork();
        worker[0].goToVocation((int) (Math.random() * 10) + 10);

        System.out.println(worker[1]);
        worker[1].goToWork();
        worker[1].goToVocation((int) (Math.random() * 10) + 10);

        System.out.println(worker[2]);
        worker[2].goToWork();
        worker[2].goToVocation((int) (Math.random() * 10) + 10);

        System.out.println(worker[3]);
        worker[3].goToWork();
        worker[3].goToVocation((int) (Math.random() * 10) + 10);


    }

}
