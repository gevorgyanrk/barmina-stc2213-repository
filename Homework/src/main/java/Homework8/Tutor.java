package Homework8;

public class Tutor extends Worker{


    public Tutor(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        System.out.println("Тьютер работает по 4 часа в день.");
    }



    @Override
    public void goToVocation(int days) {
        System.out.println("Отпуск тьютера " + days + " дней");

    }
}


