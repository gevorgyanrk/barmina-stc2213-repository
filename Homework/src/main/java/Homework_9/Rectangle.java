package Homework_9;

public class Rectangle extends Figure{


    public Rectangle(int x, int y) {
        super(x, y);
    }

    public int getPerimeter() {
        System.out.println("Периметр прямоугольника ");
        return 2 * (getX() + getY());
    }
}
