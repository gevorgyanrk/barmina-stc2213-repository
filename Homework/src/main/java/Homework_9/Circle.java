package Homework_9;

public class Circle extends Ellipse implements Moveable {

    public Circle(int x, int y) {
        super(x, y);
    }

    public void move(int nX, int nY) {
        System.out.println("Старые координаты круга " + getX() + " " + getY());
        setX(nX);
        setY(nY);
        System.out.println("Измененные координаты круга " + nX + " " + nY);

    }

    @Override
    public int getPerimeter() {
        double p = 3.14;
        System.out.println("Периметр круга ");
        return (int)(2*Math.PI*getX());
    }
}
