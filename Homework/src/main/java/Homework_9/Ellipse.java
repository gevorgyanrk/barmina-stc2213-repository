package Homework_9;

public class Ellipse extends Figure{

    public Ellipse(int x, int y) {
        super(x, y);
    }

    public int getPerimeter() {
        System.out.println("Периметр овала ");
        return (int)(4*(Math.PI*getX()*getY()+(getX()-getY()))/getX()+getY());
    }
}
