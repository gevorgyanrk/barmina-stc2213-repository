package Homework6;

import java.util.Random;
import java.util.Arrays;


public class Algorithms {
    public static void main(String[] args) {
        Random random = new Random();
        int[] array1 = new int[1000];
        for (int i = 0; i < array1.length; i++) {
            array1[i] = random.nextInt(100);
        }
        int[] array2 = new int[100];
        for (int value : array1) {
            array2[value]++;
        }
        minSubsequence(array2);
        System.out.println(minSubsequence(array2));
    }

    public static int minSubsequence(int[] arr) {
        int min = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < min)
                min = arr[i];
        }
        return min;
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }
}









